const { MyBigNumber } = require("../MyBigNumber");

let myBigNumber = new MyBigNumber();
// Viết các test case cho phương thức sum

describe("sum", () => {
  // Test case 1: hai số có cùng số chữ số
  test("sum of two numbers with same number of digits", () => {
    // Gọi phương thức sum với hai số "1234" và "897"
    let result = myBigNumber.sum("1234", "897");
    // Kiểm tra kết quả có bằng "2131" không
    expect(result).toBe("2131");
  });

  // Test case 2: hai số có số chữ số khác nhau
  test("sum of two numbers with different number of digits", () => {
    // Gọi phương thức sum với hai số "123" và "4567"
    let result = myBigNumber.sum("123", "4567");
    // Kiểm tra kết quả có bằng "4690" không
    expect(result).toBe("4690");
  });

  // Test case 3: hai số có nhớ khi cộng
  test("sum of two numbers with carry", () => {
    // Gọi phương thức sum với hai số "9999" và "1"
    let result = myBigNumber.sum("9999", "1");
    // Kiểm tra kết quả có bằng "10000" không
    expect(result).toBe("10000");
  });
});
